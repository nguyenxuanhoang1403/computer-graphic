
'''
Ref:
    https://nrotella.github.io/journal/first-steps-python-qt-opengl.html

    https://stackoverflow.com/questions/24788939/how-do-i-change-the-color-of-one-vertex-instead-of-all-of-them

    http://pyopengl.sourceforge.net/context/tutorials/shader_1.html

'''

from PySide2 import QtCore      # core Qt functionality
from PySide2 import QtGui       # extends QtCore with GUI functionality
from PySide2 import QtWidgets
from PySide2 import QtOpenGL    # provides QGLWidget, a special OpenGL QWidget

import OpenGL.GL as gl        # python wrapping of OpenGL
from OpenGL import GLU        # OpenGL Utility Library, extends OpenGL functionality
from OpenGL.GL import shaders

import sys                    # we'll need this later to run our Qt application

from OpenGL.arrays import vbo
import numpy as np


VERTEX_SHADER = """
    void main() {
        gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
        gl_FrontColor = gl_Color;
    }"""
#
# VERTEX_SHADER = """
#     varying vec4 vertex_color;
#     void main() {
#         gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
#         vertex_color = gl_Color;
#     }"""
#

FRAGMENT_SHADER = """void main() {
                gl_FragColor = gl_Color;
            }"""
# FRAGMENT_SHADER = """
#            varying vec4 vertex_color;
#            void main() {
#                gl_FragColor = vertex_color;
#            }"""


class GLWidget(QtOpenGL.QGLWidget):
    def __init__(self, parent=None):
        self.parent = parent
        QtOpenGL.QGLWidget.__init__(self, parent)

        self.width = None
        self.height = None

        self.rotX = 0.0
        self.rotY = 0.0
        self.rotZ = 0.0

    def initializeGL(self):
        self.shader = shaders.compileProgram(
            shaders.compileShader(VERTEX_SHADER, gl.GL_VERTEX_SHADER),
            shaders.compileShader(FRAGMENT_SHADER, gl.GL_FRAGMENT_SHADER)
        )
        # initialize the screen to a white color
        self.qglClearColor(QtGui.QColor(255, 255, 255))

        # enable depth testing
        gl.glEnable(gl.GL_DEPTH_TEST)

        self.initGeometry()



    def resizeGL(self, width, height):
        print("Resized: ", width, height)
        self.width = width
        self.height = height

        gl.glViewport(0, 0, width, height) # set the viewport

        gl.glMatrixMode(gl.GL_PROJECTION)  # set the top matrix in stack is the projection matrix
        gl.glLoadIdentity()  # set the current matrix to the Identity matrix

        # https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluPerspective.xml
        # aspect = width / float(height)
        # GLU.gluPerspective(45.0, aspect, 1.0, 100.0)

        # https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glOrtho.xml
        gl.glOrtho(-width/2, width/2, -height/2, height/2, 1.0, 100.0)

        gl.glMatrixMode(gl.GL_MODELVIEW)  # set the top matrix in stack is the model_view matrix
        gl.glLoadIdentity()

    def paintGL(self):
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

        gl.glUseProgram(self.shader)

        # draw objects
        self.draw_box_1()

        gl.glUseProgram(0)

    def draw_box_1(self):
        # push the current matrix to the current stack. The current matrix now is model_view
        gl.glPushMatrix()

        # Order of matrix multiplication below:
        #   model_view = model_view * glTranslate * glScale * glRotate * glTranslate
        # 4th: translate cube to specified depth
        gl.glTranslate(0.0, 0.0, -50.0)
        # 3rd: scale the tube
        gl.glScale(self.width / 4, self.width / 4, 1.0)
        # 2nd: rotate the cube
        gl.glRotate(self.rotX, 1.0, 0.0, 0.0)
        gl.glRotate(self.rotY, 0.0, 1.0, 0.0)
        gl.glRotate(self.rotZ, 0.0, 0.0, 1.0)
        # 1st: translate cube center to origin
        gl.glTranslate(-0.5, -0.5, -0.5)

        gl.glEnableClientState(gl.GL_VERTEX_ARRAY)
        gl.glEnableClientState(gl.GL_COLOR_ARRAY)

        gl.glVertexPointer(3, gl.GL_FLOAT, 0, self.vertVBO)
        gl.glColorPointer(3, gl.GL_FLOAT, 0, self.colorVBO)

        gl.glDrawElements(gl.GL_QUADS, len(self.cubeIdxArray), gl.GL_UNSIGNED_INT, self.cubeIdxArray)

        gl.glDisableClientState(gl.GL_VERTEX_ARRAY)
        gl.glDisableClientState(gl.GL_COLOR_ARRAY)

        gl.glPopMatrix()  # restore the previous model_view matrix

    def initGeometry(self):
        # Create VBO for vertex coordinates and color at each vertex
        self.cubeVtxArray = np.array(
                [[0.0, 0.0, 0.0],
                 [1.0, 0.0, 0.0],
                 [1.0, 1.0, 0.0],
                 [0.0, 1.0, 0.0],
                 [0.0, 0.0, 1.0],
                 [1.0, 0.0, 1.0],
                 [1.0, 1.0, 1.0],
                 [0.0, 1.0, 1.0]])
        self.vertVBO = vbo.VBO(np.reshape(self.cubeVtxArray, (1, -1)).astype(np.float32))
        self.vertVBO.bind()

        self.cubeClrArray = np.array(
                [[0.0, 0.0, 0.0],
                 [1.0, 0.0, 0.0],
                 [1.0, 1.0, 0.0],
                 [0.0, 1.0, 0.0],
                 [0.0, 0.0, 1.0],
                 [1.0, 0.0, 1.0],
                 [1.0, 1.0, 1.0],
                 [0.0, 1.0, 1.0]])
        self.colorVBO = vbo.VBO(np.reshape(self.cubeClrArray, (1, -1)).astype(np.float32))
        self.colorVBO.bind()

        self.cubeIdxArray = np.array(
                [0, 1, 2, 3,
                 3, 2, 6, 7,
                 1, 0, 4, 5,
                 2, 1, 5, 6,
                 0, 3, 7, 4,
                 7, 6, 5, 4])

    def setRotX(self, val):
        self.rotX = np.pi * val
        self.updateGL()

    def setRotY(self, val):
        self.rotY = np.pi * val
        self.updateGL()

    def setRotZ(self, val):
        self.rotZ = np.pi * val
        self.updateGL()

    def delete_vbos(self):
        # we must clean vbos before closing the program
        self.vertVBO.delete()
        self.colorVBO.delete()

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)    # call the init for the parent class

        self.resize(300, 300)
        self.setWindowTitle('Hello OpenGL App')

        self.glWidget = GLWidget(self)
        self.initGUI()

    def initGUI(self):
        central_widget = QtWidgets.QWidget()
        gui_layout = QtWidgets.QVBoxLayout()
        central_widget.setLayout(gui_layout)

        self.setCentralWidget(central_widget)

        gui_layout.addWidget(self.glWidget)

        sliderX = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        sliderX.valueChanged.connect(lambda val: self.glWidget.setRotX(val))

        sliderY = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        sliderY.valueChanged.connect(lambda val: self.glWidget.setRotY(val))

        sliderZ = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        sliderZ.valueChanged.connect(lambda val: self.glWidget.setRotZ(val))

        gui_layout.addWidget(sliderX)
        gui_layout.addWidget(sliderY)
        gui_layout.addWidget(sliderZ)

    def closeEvent(self, *args, **kwargs):
        self.glWidget.delete_vbos()


if __name__ == '__main__':

    app = QtWidgets.QApplication(sys.argv)

    win = MainWindow()
    win.show()

    sys.exit(app.exec_())

