import sys
import numpy as np
import collections as cl
import contextlib
import ctypes
from ctypes import c_void_p

import qt
import lite_mesh_tools as lmt, exo_data as ed
import gl
import bounding_box as bb


null = c_void_p(0)
sizeOfFloat = ctypes.sizeof(gl.GLfloat)
AMBIENT = np.array([1.0, 0.6, 0.8, 1.0], dtype=np.float32)
DIFFUSE = np.array([0.5, 0.5, 0.5, 0.0], dtype=np.float32)
SPECULAR = np.array([0.5, 0.3, 0.14, 0.3], dtype=np.float32)
EMISSION = np.array([0.1, 0.1, 0.14, 0.0], dtype=np.float32)
LIGHT_POS = np.array([0.0, 1.0, 0.0, 0.0], dtype=np.float32)
LIGHT_COLOR = np.array([0.1, 1.0, 0.2, 0.0], dtype=np.float32)
COLOR_RANGE = np.array(
    [(0.0, 0.0, 0.7),
     (0.0, 0.0, 0.9),
     (0.0, 0.25, 1.0),
     (0.0, 0.5, 1.0),
     (0.0, 0.75, 1.0),
     (0.0, 1.0, 1.0),
     (0.25, 1.0, 0.5),
     (0.75, 1.0, 0.25),
     (1.0, 1.0, 0.0),
     (1.0, 0.75, 0.0),
     (1.0, 0.5, 0.0),
     (1.0, 0.25, 0.0),
     (0.75, 0.0, 0.0),
     (0.5, 0.0, 0.0)], dtype=np.float32)
SHININESS = 100
NUMBER_OF_BINS = len(COLOR_RANGE)
LOW_COLOR = np.array((0.0, 0.0, 0.7), dtype=np.float32)
HIGH_COLOR = np.array((0.5, 0.0, 0.0), dtype=np.float32)

ColoredRectangle = cl.namedtuple('ColoredRectangle', ['x', 'y', 'width', 'height', 'color'])
TextPosition = cl.namedtuple('TextPosition', ['x', 'y', 'text'])


def get_screen_font(scale):
    # Use some heuristic to scale font size, I dunno.
    size = 10 * scale

    is_italic = False
    return qt.QFont("Open Sans", size, qt.QFont.Normal, is_italic)


def get_data_pointer(arr):
    assert isinstance(arr, np.ndarray)
    return gl.ad.voidDataPointer(arr)


def get_data_type(arr):
    assert isinstance(arr, np.ndarray)
    return gl.nm.ARRAY_TO_GL_TYPE_MAPPING[arr.dtype.type(0).dtype]


def get_data_size(arr):
    assert isinstance(arr, np.ndarray)
    return gl.ad.arrayByteCount(arr)  # len(vertexPositions) * sizeOfFloat


def convert_scalar_value_to_rgb_color(scalar_values, mn=None, mx=None, is_discrete_colors=False):
    assert isinstance(scalar_values, np.ndarray)
    final_rgba_color_arr = np.full((len(scalar_values), 3), 0.0, dtype=np.float32)
    inbetween_indexes = None
    if mn is not None and mx is not None:
        inbetween_indexes = np.intersect1d(
            np.where(scalar_values <= mx), np.where(scalar_values >= mn))
        final_rgba_color_arr[np.where(scalar_values < mn)] = LOW_COLOR
        final_rgba_color_arr[np.where(scalar_values > mx)] = HIGH_COLOR
    elif mn is None and mx is None:
        mn = np.min(scalar_values)
        mx = np.max(scalar_values)
        inbetween_indexes = np.arange(len(scalar_values)).tolist()
    else:
        ValueError('Invalid input mx, mn')
    scalar_range = np.linspace(mn, mx, len(COLOR_RANGE) + 1)
    for idx in inbetween_indexes:
        bin_index = 0
        for i in range(NUMBER_OF_BINS):
            if scalar_values[idx] <= scalar_range[i + 1]:
                bin_index = i
                break
        if is_discrete_colors:
            final_rgba_color_arr[idx] = COLOR_RANGE[bin_index]
        else:
            bin_size = scalar_range[bin_index + 1] - scalar_range[bin_index]
            bin_center = scalar_range[bin_index] + 0.5 * bin_size
            interpolator = 0.0
            other_bin_index = bin_index
            if scalar_values[idx] > bin_center and bin_index < NUMBER_OF_BINS - 1:
                interpolator = (scalar_values[idx] - bin_center) / bin_size
                other_bin_index = bin_index + 1
            elif scalar_values[idx] <= bin_center and bin_index > 0:
                interpolator = (bin_center - scalar_values[idx]) / bin_size
                other_bin_index = bin_index - 1
            #  https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/mix.xhtml
            final_rgba_color_arr[idx] = COLOR_RANGE[bin_index] * (
                    1 - interpolator) + COLOR_RANGE[other_bin_index] * interpolator
    return final_rgba_color_arr


class Shading:
    VERTEX_SHADER = """#version 120
            attribute vec3 vertex;
            attribute vec3 normal;
            attribute vec3 color;
            varying vec4 vertex_color;
            uniform bool draw_edges=false;
            uniform mat4 model_view;
            uniform mat4 projection;
            void main() {
                gl_Position = projection * model_view * vec4(vertex, 1.0);         
                if (draw_edges) {
                    // draw element edges
                    vertex_color = vec4(0.0, 0.0, 0.0, 1.0);
                }
                else {
                    // draw scalar field value
                    vertex_color = vec4(color, 1.0);
                }
            }
        """
    FRAGMENT_SHADER = """#version 120
            varying vec4 vertex_color;
            void main() {
                //gl_FragColor = vec4(1.0, 1.0, 0.0, 1.0);
                gl_FragColor = vertex_color;
            }
        """

    class AttribInfo:
        def __init__(self, location_idx: int, n_components: int):
            self.location_idx = location_idx
            self.n_components = n_components

    class ShaderType:
        def __init__(self, s_vert, s_frag, attrib_names):
            self.s_vert = s_vert
            self.s_frag = s_frag
            self.attrib_names = attrib_names

    ATTRIB_INFOS = {
        "vertex": AttribInfo(0, 3),
        "normal": AttribInfo(1, 3),
        "color": AttribInfo(2, 3),
    }

    SHADER_TYPE = ShaderType(VERTEX_SHADER, FRAGMENT_SHADER, ['vertex', 'vertex_color'])


sd = Shading  # shortcut


class MainWindow(qt.QMainWindow):

    def __init__(self):
        qt.QMainWindow.__init__(self)  # call the init for the parent class

        self.resize(300, 300)
        self.setWindowTitle('Hello OpenGL App')
        self.glWidget = GLWidget(self)
        self.initGUI()

    def initGUI(self):
        central_widget = qt.QWidget()
        gui_layout = qt.QVBoxLayout()
        central_widget.setLayout(gui_layout)

        self.setCentralWidget(central_widget)

        gui_layout.addWidget(self.glWidget)

        sliderX = qt.QSlider(qt.Qt.Horizontal)
        sliderX.valueChanged.connect(lambda val: self.glWidget.setRotX(val))

        sliderY = qt.QSlider(qt.Qt.Horizontal)
        sliderY.valueChanged.connect(lambda val: self.glWidget.setRotY(val))

        sliderZ = qt.QSlider(qt.Qt.Horizontal)
        sliderZ.valueChanged.connect(lambda val: self.glWidget.setRotZ(val))

        sliderColor = qt.QSlider(qt.Qt.Horizontal)
        sliderColor.valueChanged.connect(lambda val: self.glWidget.changeColor(val))

        gui_layout.addWidget(sliderX)
        gui_layout.addWidget(sliderY)
        gui_layout.addWidget(sliderZ)
        gui_layout.addWidget(qt.QLabel('Change Color'))
        gui_layout.addWidget(sliderColor)

    def closeEvent(self, *args, **kwargs):
        self.glWidget.delete_vbos()


class GLWidget(qt.QOpenGLWidget):
    def __init__(self, parent):
        qt.QOpenGLWidget.__init__(self, parent)
        # qt.QOpenGLWidget also works but use update instead of updateGL
        self.setMinimumSize(640, 480)

        self.n_vertex_components = 3  # x, y, z

        self.vao_id = None
        self.gl_buffer_id = None

        self.program = None
        self.triad_program = None
        self.uniform_variables = {}

        #
        self.width = None
        self.height = None
        #
        self.rotX = 0.0
        self.rotY = 0.0
        self.rotZ = 0.0
        self.bounding_box = bb.BoundingBox.create(VERTICES)
        self.painter = qt.QPainter()
        self.scale = 1.0
        self.translation_vec = np.array([0., 0., 0.])
        self.last_mouse_x = 0
        self.last_mouse_y = 0
        self.frame_buffer_id = None
        self.pick_gl_id = None
        self.depth_gl_id = None
        self.current_pick_idx = 0

    def initializeGL(self):
        print("Initialized GL")
        # The index starts from 1
        self.vao_id = gl.glGenVertexArrays(1)
        self.buffer_ids = gl.glGenBuffers(8)

        self.initialize_program()

        self.initialize_vertex_buffers()

    def resizeGL(self, width, height):
        print("Resized: ", width, height)
        self.create_frame_buffer(width, height)
        self.width = width
        self.height = height
        gl.glViewport(0, 0, width, height)  # set the viewport
        gl.glClear(gl.GL_DEPTH_BUFFER_BIT)
        aspect = width / height
        diag = self.bounding_box.get_diag()
        projection_matrix = get_projection_matrix(-aspect * diag, aspect * diag, -diag, diag, 0.1, 2 * diag)
        gl.glUseProgram(self.program.programId())
        gl.glUniformMatrix4fv(
            self.uniform_variables['projection'], 1, gl.GL_FALSE, get_data_pointer(projection_matrix))
        model_view_matrix = np.ascontiguousarray(np.identity(4), dtype=np.float32)
        gl.glUniformMatrix4fv(
            self.uniform_variables['model_view'], 1, gl.GL_FALSE, get_data_pointer(model_view_matrix))
        gl.glUseProgram(0)

    def paintGL(self):
        self.clear()
        gl.glDepthMask(gl.GL_TRUE)
        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glEnable(gl.GL_BLEND)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
        self.draw(COLORS)
        if self.current_pick_idx != 0:
            gl.glViewport(0, 0, self.width, self.height)
            self.draw(PICK_COLORS)
        gl.glViewport(0, 0, self.width, self.height)
        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, self.frame_buffer_id)
        self.draw(PICK_COLORS)
        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, 0)
        gl.glViewport(0, 0, self.width, self.height)
        # Note: render text should be called after the 3D render is done
        default_height = 1200.
        desktop_height = qt.QApplication.desktop().screenGeometry().height()
        scale = desktop_height / default_height
        field_name = 'Displacement x'
        self.render_color_bar(scale, field_name)
        self.render_triad_axis_names(scale)

    def draw(self, color):
        gl.glUseProgram(self.program.programId())
        aspect = self.width / self.height
        diag = self.bounding_box.get_diag()
        proj = get_projection_matrix(-aspect * diag, aspect * diag, -diag, diag, - 2 * diag, 2 * diag)
        gl.glUniformMatrix4fv(
            self.uniform_variables['projection'], 1, gl.GL_FALSE, get_data_pointer(proj))
        gl.glBindVertexArray(self.vao_id)
        scaling_matrix = get_scaling_matrix(self.scale)
        translation_vec = self.translation_vec + np.array(
            [0, 0, -self.bounding_box.get_diag()])
        translation_matrix = np.identity(4, dtype=np.float32)
        translation_matrix[:3, 3] = translation_vec
        model_view_matrix = get_model_view_matrix(self.rotX, self.rotY, self.rotZ)
        net_model_view_matrix = np.dot(scaling_matrix, np.dot(translation_matrix, model_view_matrix))
        net_model_view_matrix = net_model_view_matrix.transpose()
        final_model_view_matrix = np.ascontiguousarray(net_model_view_matrix, dtype=np.float32)
        gl.glUniformMatrix4fv(
            self.uniform_variables['model_view'], 1, gl.GL_FALSE, get_data_pointer(final_model_view_matrix))
        # Bind vertex coord buffer and enable the corresponding vertex attribute array
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.buffer_ids[0])
        vertex_location = 0
        gl.glVertexAttribPointer(
            vertex_location, self.n_vertex_components, get_data_type(VERTICES), gl.GL_FALSE, 0,
            null)
        gl.glEnableVertexAttribArray(vertex_location)
        # Bind normal buffer and enable the corresponding vertex attribute array
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.buffer_ids[1])
        normal_location = 1
        gl.glVertexAttribPointer(
            normal_location, self.n_vertex_components, get_data_type(NORMALS), gl.GL_FALSE, 0, null)
        gl.glEnableVertexAttribArray(normal_location)
        # Bind vertex color buffer and enable the corresponding vertex attribute array
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.buffer_ids[2])
        color_location = 2
        gl.glVertexAttribPointer(color_location, self.n_vertex_components, get_data_type(color), gl.GL_FALSE, 0, null)
        gl.glEnableVertexAttribArray(color_location)
        self.bind_buffer(gl.GL_ARRAY_BUFFER, self.buffer_ids[2], color)
        # Draw elems
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, self.buffer_ids[3])
        gl.glUniform1f(self.uniform_variables['draw_edges'], False)
        gl.glDrawElements(gl.GL_TRIANGLES, len(INDICES), get_data_type(INDICES), null)
        # Draw edges
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, self.buffer_ids[4])
        gl.glUniform1f(self.uniform_variables['draw_edges'], True)
        gl.glLineWidth(1)
        gl.glDrawElements(gl.GL_LINES, len(LINE_INDICES), get_data_type(LINE_INDICES), null)
        self.render_triad()
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, 0)
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, 0)
        gl.glBindVertexArray(0)

        gl.glUseProgram(0)

    def initialize_program(self):
        program = qt.QOpenGLShaderProgram(self.context())
        shader_type = sd.SHADER_TYPE  # which is a list of shader types in GUI
        if not program.addShaderFromSourceCode(qt.QOpenGLShader.Vertex, shader_type.s_vert):
            print(program.log())

        if not program.addShaderFromSourceCode(qt.QOpenGLShader.Fragment, shader_type.s_frag):
            print(program.log())

        gl.glBindAttribLocation(program.programId(), 0, 'vertex')
        gl.glBindAttribLocation(program.programId(), 1, 'normal')
        gl.glBindAttribLocation(program.programId(), 2, 'color')

        program.link()
        program.bind()
        self.program = program
        self.uniform_variables['draw_edges'] = gl.glGetUniformLocation(
            self.program.programId(), 'draw_edges')
        self.uniform_variables['model_view'] = gl.glGetUniformLocation(
            self.program.programId(), 'model_view')
        self.uniform_variables['projection'] = gl.glGetUniformLocation(
            self.program.programId(), 'projection')

    def initialize_vertex_buffers(self):
        # TODO: the reference 1 mentions that the GL_ELEMENT_ARRAY_BUFFER should be the last bind
        gl.glBindVertexArray(self.vao_id)

        # bind vertex coordinates to buffer 1
        self.bind_buffer(gl.GL_ARRAY_BUFFER, self.buffer_ids[0], VERTICES)
        # bind colors to buffer 2
        self.bind_buffer(gl.GL_ARRAY_BUFFER, self.buffer_ids[1], NORMALS)
        # bind colors to buffer 3
        self.bind_buffer(gl.GL_ARRAY_BUFFER, self.buffer_ids[2], COLORS)
        # bind vertex indices to buffer 4
        self.bind_buffer(gl.GL_ELEMENT_ARRAY_BUFFER, self.buffer_ids[3], INDICES)
        self.bind_buffer(gl.GL_ELEMENT_ARRAY_BUFFER, self.buffer_ids[4], LINE_INDICES)
        # bind vertex, indices, colors of triad to buffers
        self.bind_buffer(gl.GL_ARRAY_BUFFER, self.buffer_ids[5], TRIAD_VERTICES)
        self.bind_buffer(gl.GL_ARRAY_BUFFER, self.buffer_ids[6], TRIAD_COLORS)
        self.bind_buffer(gl.GL_ELEMENT_ARRAY_BUFFER, self.buffer_ids[7], TRIAD_INDICES)
        # reset to default should be in the following order
        gl.glBindVertexArray(0)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, 0)
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, 0)

    def bind_buffer(self, target, gl_buffer_id, arr):
        # target: which the buffer object is bound
        gl.glBindBuffer(target, gl_buffer_id)
        gl.glBufferData(target, get_data_size(arr), get_data_pointer(arr), gl.GL_STATIC_DRAW)
        # glBindBuffer: binds a buffer object to the specified buffer binding point
        # glBufferData: copies the data to GPU's buffer
        gl_error = gl.glGetError()
        if gl_error == gl.GL_OUT_OF_MEMORY:
            raise MemoryError()
        elif gl_error != gl.GL_NO_ERROR:
            raise ValueError()

    def render_color_bar(self, scale, field_name):
        font = get_screen_font(scale)
        self.painter.begin(self)
        self.painter.beginNativePainting()
        self.painter.setPen(qt.Qt.black)
        self.painter.setFont(font)
        rectangles, texts, underscore_texts = create_legend_rectangles(
            scale, COLOR_RANGE[::-1], NODE_FIELD_VALUES, field_name)
        # draw text Field
        self.painter.drawText(texts[0].x, texts[0].y, texts[0].text)
        for text, underscore_text in zip(texts[1:], underscore_texts):
            self.painter.drawText(text.x, text.y, text.text)
            self.painter.drawText(underscore_text.x, underscore_text.y, underscore_text.text)
        self.painter.setPen(qt.Qt.NoPen)
        for rectangle in rectangles:
            self.painter.setBrush(qt.QColor.fromRgbF(*rectangle.color))
            self.painter.drawRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height)
        self.painter.endNativePainting()
        self.painter.end()

    def render_triad_axis_names(self, scale):
        font = get_screen_font(scale)
        self.painter.begin(self)
        self.painter.beginNativePainting()
        x = 25 * scale
        y = self.height - 25 * scale
        color_x = [0.8, 0.2, 0.2]
        color_y = [0.2, 0.8, 0.2]
        color_z = [0.2, 0.2, 0.8]
        colors = [color_x, color_y, color_z]
        self.painter.setFont(font)
        for idx, (text, color) in enumerate(zip(list('xyz'), colors)):
            self.painter.setPen(qt.QColor.fromRgbF(*color))
            self.painter.drawText(x + (x * idx)/2, y, text)
        self.painter.endNativePainting()
        self.painter.end()

    def render_triad(self):
        gl.glUniform1f(self.uniform_variables['draw_edges'], False)
        gl.glViewport(0, 0, 100, 100)
        gl.glClear(gl.GL_DEPTH_BUFFER_BIT)
        projection_matrix = get_projection_matrix(-10., 10., -10., 10., -100., 100.)
        gl.glUniformMatrix4fv(
            self.uniform_variables['projection'], 1, gl.GL_FALSE, get_data_pointer(projection_matrix))
        triad_model_view = np.ascontiguousarray(np.identity(4), dtype=np.float32)
        triad_model_view[0, 3] = 1
        triad_model_view[1, 3] = 1
        model_view_matrix = get_model_view_matrix(self.rotX, self.rotY, self.rotZ)
        net_model_view_matrix = np.dot(triad_model_view, model_view_matrix)
        net_model_view_matrix = np.ascontiguousarray(net_model_view_matrix.transpose(), dtype=np.float32)
        gl.glUniformMatrix4fv(
            self.uniform_variables['model_view'], 1, gl.GL_FALSE, get_data_pointer(net_model_view_matrix))
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.buffer_ids[5])
        vertex_location = 0
        gl.glVertexAttribPointer(
            vertex_location, self.n_vertex_components, get_data_type(TRIAD_VERTICES), gl.GL_FALSE, 0, null)
        gl.glEnableVertexAttribArray(vertex_location)
        #
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.buffer_ids[6])
        color_location = 2
        gl.glVertexAttribPointer(
            color_location, self.n_vertex_components,
            get_data_type(TRIAD_COLORS), gl.GL_FALSE, 0, null)
        gl.glEnableVertexAttribArray(color_location)
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, self.buffer_ids[7])
        gl.glLineWidth(5)
        gl.glDrawElements(gl.GL_LINES, len(TRIAD_INDICES), get_data_type(TRIAD_INDICES), null)

    def clear(self):
        gl.glClearColor(1.0, 1.0, 1.0, 0.0)
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        gl.glLoadIdentity()

    def create_frame_buffer(self, width, height):
        if self.frame_buffer_id is not None:
            gl.glDeleteFramebuffers(1, [self.frame_buffer_id])
        if self.pick_gl_id is not None:
            gl.glDeleteTextures([self.pick_gl_id])
        if self.depth_gl_id is not None:
            gl.glDeleteTextures([self.depth_gl_id])
        self.pick_gl_id = gl.glGenTextures(1)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.pick_gl_id)
        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_RGBA, width, height, 0,
                        gl.GL_RGBA, gl.GL_UNSIGNED_BYTE, None)

        gl.glBindTexture(gl.GL_TEXTURE_2D, 0)
        self.depth_gl_id = gl.glGenTextures(1)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.depth_gl_id)
        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_DEPTH_COMPONENT, width,
                        height, 0, gl.GL_DEPTH_COMPONENT, gl.GL_FLOAT, None)

        gl.glBindTexture(gl.GL_TEXTURE_2D, 0)
        self.frame_buffer_id = gl.glGenFramebuffers(1)
        print('frame_buffer_id', self.frame_buffer_id)
        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, self.frame_buffer_id)
        gl.glFramebufferTexture2D(gl.GL_FRAMEBUFFER, gl.GL_COLOR_ATTACHMENT0,
                                  gl.GL_TEXTURE_2D, self.pick_gl_id, 0)
        gl.glFramebufferTexture2D(gl.GL_FRAMEBUFFER, gl.GL_DEPTH_ATTACHMENT,
                                  gl.GL_TEXTURE_2D, self.depth_gl_id, 0)
        status = gl.glCheckFramebufferStatus(gl.GL_FRAMEBUFFER)
        assert status == gl.GL_FRAMEBUFFER_COMPLETE, 'Invalid status: {}'.format(status)

        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, 0)

    def read_pixels(self, x, y):
        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, self.frame_buffer_id)
        pixels = gl.glReadPixels(x, y, 1, 1, gl.GL_RGB, gl.GL_FLOAT)
        rgb = [int(v) for v in np.round(255.0 * pixels[0, 0])]
        idx = decode_padic(rgb)
        print('RGB:', rgb)
        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, 0)
        return idx

    def delete_vbos(self):
        gl.glDeleteBuffers(len(self.buffer_ids), self.buffer_ids)

    def get_current_model_view_matrix(self):
        # Force to get a contiguous array in memory (C order)
        # We need a transpose here due to the current matrix stored in C-code in column major order.
        # Whereas, it is row major order in Python-code
        return np.ascontiguousarray(gl.glGetFloatv(gl.GL_MODELVIEW_MATRIX),
                                    dtype=np.float32).transpose()

    def mouseMoveEvent(self, event):
        if event.buttons() & qt.Qt.MiddleButton:
            delta_x = event.x() - self.last_mouse_x
            delta_y = self.last_mouse_y - event.y()
            if abs(delta_x) + abs(delta_y) == 0:
                return
            mouse_vec = np.array([delta_x, delta_y, 0.])
            eye_vec = np.dot(self.get_current_model_view_matrix()[: 3, :3], EYE_VEC)
            up_vec = np.cross(mouse_vec, eye_vec)
            up_vec = up_vec / np.linalg.norm(up_vec)
            angle = 360 * np.sqrt(delta_x ** 2 + delta_y ** 2) / self.width
        self.last_mouse_x = event.x()
        self.last_mouse_y = event.y()

    def mousePressEvent(self, event):
        if event.button() & qt.Qt.LeftButton:
            mouse_vec = np.array([event.x() - self.last_mouse_x, self.last_mouse_y - event.y(), 0]) / 1000
            # self.translation_vec = mouse_vec
        self.current_pick_idx = self.read_pixels(event.x(), event.y())
        self.last_mouse_x = event.x()
        self.last_mouse_y = event.y()
        self.update()

    def wheelEvent(self, event):
        sx = event.angleDelta().y() / 1000
        self.scale += sx
        self.update()

    def setRotX(self, val):
        self.rotX = np.pi * val
        self.update()

    def setRotY(self, val):
        self.rotY = np.pi * val
        self.update()

    def setRotZ(self, val):
        self.rotZ = np.pi * val
        self.update()

    def changeColor(self, val):
        global COLORS
        for i in range(len(COLORS)):
            COLORS[i] = [np.random.uniform(), np.random.uniform(), np.random.uniform()]
            # Note: the COLORS has been changed, so it must be copied to buffer again
            self.bind_buffer(gl.GL_ARRAY_BUFFER, self.buffer_ids[2], COLORS)
        self.update()


def create_legend_rectangles(scale, color_levels, scalar_values, field_name):
    mn = np.min(scalar_values)
    mx = np.max(scalar_values)
    scalar_delta = (mx - mn) / len(color_levels)
    rectangles = []
    texts = []
    underscore_texts = []
    width = 50 * scale
    height = 25 * scale
    origin = [width / 2, 75 * scale]
    color_label = f'Field: {field_name}'
    texts.append(TextPosition(origin[0], origin[1] - height, color_label))
    for idx, color in enumerate(color_levels):
        x = origin[0]
        y = origin[1] + height * idx
        rect = ColoredRectangle(x, y, width, height, color)
        rectangles.append(rect)
    for idx in range(len(color_levels) + 1):
        x = origin[0]
        y = origin[1] + height * idx
        underscore_text = TextPosition(x + width, y, '_')
        #TODO: check text value of color bar
        text = TextPosition(x + 1.5 * width, y, str(mx - scalar_delta * idx))
        texts.append(text)
        underscore_texts.append(underscore_text)
    return rectangles, texts, underscore_texts


def get_projection_matrix(l, r, b, t, n, f):
    proj = np.identity(4, dtype=np.float32)
    proj[0, 0] = 2 / (r - l)
    proj[1, 1] = 2 / (t - b)
    proj[2, 2] = - 2 / (f - n)
    proj[0, 3] = -(r + l) / (r - l)
    proj[1, 3] = -(t + b) / (t - b)
    proj[2, 3] = -(f + n) / (f - n)
    return np.ascontiguousarray(proj.transpose(), dtype=np.float32)


def get_model_view_matrix(rot_x, rot_y, rot_z):
    rot_x = np.radians(rot_x)
    rot_y = np.radians(rot_y)
    rot_z = np.radians(rot_z)
    rot_matrix_x = np.identity(4, dtype=np.float32)
    rot_matrix_y = np.identity(4, dtype=np.float32)
    rot_matrix_z = np.identity(4, dtype=np.float32)
    rot_matrix_x[1, 1] = np.cos(rot_x)
    rot_matrix_x[1, 2] = - np.sin(rot_x)
    rot_matrix_x[2, 1] = np.sin(rot_x)
    rot_matrix_x[2, 2] = np.cos(rot_x)
    rot_matrix_y[0, 0] = np.cos(rot_y)
    rot_matrix_y[0, 2] = np.sin(rot_y)
    rot_matrix_y[2, 0] = - np.sin(rot_y)
    rot_matrix_y[2, 2] = np.cos(rot_y)
    rot_matrix_z[0, 0] = np.cos(rot_z)
    rot_matrix_z[0, 1] = - np.sin(rot_z)
    rot_matrix_z[1, 0] = np.sin(rot_z)
    rot_matrix_z[1, 1] = np.cos(rot_z)
    x_y = np.dot(rot_matrix_x, rot_matrix_y)
    m_v = np.dot(x_y, rot_matrix_z)
    return np.ascontiguousarray(m_v, dtype=np.float32)


def get_scaling_matrix(s):
    scaling_matrix = np.identity(4, dtype=np.float32)
    scaling_matrix[0, 0] = s
    scaling_matrix[1, 1] = s
    scaling_matrix[2, 2] = s
    return np.ascontiguousarray(scaling_matrix, dtype=np.float32)


def get_triad():
    origin = [0., 0., 0.]
    direction_x = [5., 0., 0.]
    direction_y = [0., 5., 0.]
    direction_z = [0., 0., 5.]
    color_x = [0.8, 0.2, 0.2]
    color_y = [0.2, 0.8, 0.2]
    color_z = [0.2, 0.2, 0.8]
    vertices = np.ascontiguousarray(np.array([origin, direction_x, origin,
                                              direction_y, origin, direction_z]), np.float32)
    colors = np.ascontiguousarray(np.array([color_x, color_x, color_y,
                                            color_y, color_z, color_z]), np.float32)
    indices = np.ascontiguousarray(np.array([[0, 1], [2, 3], [4, 5]]).flatten(), dtype=np.uint32)
    return vertices, colors, indices


def decode_padic(rgb, b=256, idx=0):
    m = 1
    for i in range(3):
        idx += m * rgb[i]
        m *= b
    return idx


@contextlib.contextmanager
def my_application():
    app = qt.QApplication(sys.argv)
    yield
    app.exec_()


if __name__ == '__main__':
    exo_file = r'E:\Research\learn_qt\learn_OpenGL\exo_files\0_global_solution_000_component_0_surfaceOnly.exo'
    exo_data = ed.ExoData.read(exo_file)
    surface_exo_data, _ = lmt.get_surface_mesh(exo_data)
    NODE_FIELD_VALUES = surface_exo_data.node_field_values['u'][0]
    NODE_FIELD_VALUES = NODE_FIELD_VALUES
    VERTICES = np.ascontiguousarray(surface_exo_data.coords, dtype=np.float32)
    INDICES = np.ascontiguousarray(surface_exo_data.exo_block_datas[0].elems.flatten(), dtype=np.uint32)
    NORMALS = np.ascontiguousarray([[0, 0, 1] for _ in range(len(VERTICES))], dtype=np.float32)
    COLORS = convert_scalar_value_to_rgb_color(NODE_FIELD_VALUES, is_discrete_colors=False)
    PICK_COLORS = np.ascontiguousarray(np.tile(np.array([0.86, 1.0, 0.15]), len(COLORS)).reshape(-1, 3), dtype=np.float32)
    boundary_sedges = lmt.get_boundary_sedges(exo_data)
    LINE_INDICES = np.array(boundary_sedges.flatten(), dtype=np.uint32)
    bounding_box = bb.BoundingBox.create(VERTICES)
    TRIAD_VERTICES, TRIAD_COLORS, TRIAD_INDICES = get_triad()
    with my_application():
        window = MainWindow()
        window.show()
'''
Refs:
1. https://learnopengl.com/Getting-started/Hello-Triangle
'''

'''
NOTES:
- Enabling culling face as below will speedup the shading process,
        gl.glEnable(gl.GL_CULL_FACE)
        gl.glCullFace(gl.GL_BACK)   
    Since back/front faces will be removed from the shading process.
    But it is not practical when drawing shell objects, where both back and front faces are always visible.
    Finally, only use culling face when drawing closed polygon objects and the camera is not inside those objects
- Enabling depth-buffer test is very practical but also very expensive.
- Always using "np.ascontiguousarray" to get contiguous arrays with data layout in C-order. 
    The fact that the "coords" in exodus sometimes is in Fortran-order, while OpenGL requires arrays in C-order. I dunno why!
'''
